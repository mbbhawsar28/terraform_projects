variable "region" {
    description = "AWS Region"
  
}
variable "access_key" {
    description = "Access Key to the AWS"
  
}
variable "secret_key" {
    description = "Secret key to the AWS"
  
}